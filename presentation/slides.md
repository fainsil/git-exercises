---
marp: true
paginate: true
math: katex
theme: default
---

<!-- theme_diapo : https://unpkg.com/mermaid@9.1.7/dist/mermaid.min.js -->
<script src="https://unpkg.com/mermaid@9.1.7/dist/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

<style global>
section::after {
  content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
}
</style>

<style>
img[alt~="center"] {
    display: block;
    margin: 0 auto;
}
</style>

<header>

# Formation net7
</header>


# Formation gîte
![image](Pougne_gite_eg.jpg)
<footer>
2023-2024, Auchère Nathan
</footer>

---

<header>

# Introduction
</header>

##  Un gîte, c'est quoi ?

- Un hébergement touristique pour vacanciers traditionnellement installé à la campagne
- Essort avec l'exode rural d'après guerre
- Actuellement le secteur du tourisme rural grandit
- Différents labels écologiques

---

<header>

# Introduction
</header>

##  Comment le mettre sur Airbnb

1. Créer une annonce avec **photo** et **prix**
2. Renseigner les différentes caractéristiques du logement (type, chambres, salle de bain, etc)
3. Rédiger une description *efficace* et *attrayante*
4. Profiter de la moula

---

<header>

# Formation net7
</header>


![image](git.svg)
<footer>
2023-2024, Auchère Nathan
</footer>

---

<header>

# Introduction
</header>

##  Git, c'est quoi ?

- Un gestionnaire de versions décentralisé
- Créé en 2005 par Linus Torvalds, le créateur de Linux 
- Gratuit et [open source](https://github.com/git/git) (GPLv2)
- Rapide, fiable et scalable
- [Incontournable](https://trends.google.com/trends/explore?q=git,svn,mercurial), surtout dans le monde professionnel
- Un graphe !

---

<header>

# Introduction
</header>

## [Protocoles de communication](https://git-scm.com/book/en/v2/Git-on-the-Server-The-Protocols)

Git peut utiliser quatre protocoles distincts pour transférer des données:

- Local: `file://`
- HTTP: `http(s)://` (port 80)
- Secure Shell: `ssh://` (port 22)
- Git: `git://` (port 9418)

---

<header>

# Introduction
</header>

<center>

## Un Graphe
</center>


<div class="mermaid">
%%{init: { 'theme': 'base' } }%%
gitGraph
  commit
  commit
  commit
  branch toto
  checkout toto
  commit
  commit
  checkout main
  commit
  checkout toto
  commit
  checkout main
  merge toto
  commit
  branch tutu
  checkout tutu
  commit
  branch titi
  checkout titi
  commit
  checkout main
  commit
  checkout titi
  commit
  checkout tutu
  merge titi
  commit
  checkout main
  merge tutu
  commit
</div>

---
<header>

# Les bases de Git
</header>

<center>

# [Les quatre phases de git](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_recording_changes_to_the_repository)
</center>

<div class="mermaid">
sequenceDiagram
  participant Untracked
  participant Unmodified
  participant Modified
  participant Staged
  Untracked->>Staged: Add the file
  Unmodified->>Modified: Edit the file
  Modified->>Staged: Stage the file
  Unmodified->>Untracked: Remove the file
  Staged->>Unmodified: Commit
</div>

---

<header>

# Aller plus loin
</header>

## Les frontends

- [GitHub](https://github.com/)
- [GitLab](https://gitlab.com)
- [Gitea](https://gitea.io/)
- [Gogs](https://gogs.io/)
- [Codeberg](https://codeberg.org/)
- [SourceHut](https://sr.ht/)

---
<header>

# Aller plus loin
</header>

## Quelques outils

- [pre-commit](https://pre-commit.com/)
- [GitKraken](https://www.gitkraken.com/)
- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [CI/CD](https://resources.github.com/ci-cd/)
- [Signature](https://docs.github.com/en/authentication/managing-commit-signature-verification/signing-commits)
