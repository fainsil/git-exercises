# Exercice Git

Les exercices pratiques suivants devraient vous permettre de vous familiariser avec l'utilisation de Git.

Note : Les exemples de ligne de commande fournis sont là pour vous rappeler la syntaxe et les mots-clés corrects, et ne fonctionneront pas nécessairement si vous les tapez à l'aveuglette.

Autre note : pour obtenir de l'aide sur une commande git particulière, utilisez `git <commande> --help` ou `man git-<commande>`

## Installation

1. Installez et configurez git sur votre ordinateur [page du site git](https://git-scm.com/download): 
   
   Pour Linux :
   ```bash
   sudo apt/dnf/... install git
   ```

   Pour Mac 🤢 :
   ```bash
   brew install git
   ```

   pour Windows 🤮 :
   ```bash
   winget install --id Git.Git -e --source winget 
   ```

## Configuration

### Configurez son identité

   ```bash
   git config --global user.name "<Prénom Nom>"
   git config --global user.email "<identifiant@bde.enseeiht.fr>"
   ```

### Ajout d'une clé SSH

   Bien que le git de net7 le permette, la fonctionnalité permettant de se connecter en http est désactivée sur tous les autres sites. La méthode pour se connecter utilise le partage d'une clé SSH. Il faut donc en générer une et l'ajouter à son compte git.

#### Génération
   Pour générer les clés (publique et privée) :
   ```bash
   ssh-keygen -t ed25519 -C "your_email@example.com"
   ```
   Une fois enregistrées sous le nom par défaut (id_25519) ou le nom que vous avez entré, il faut afficher la clé publique :
   ```bash
   cat ~/.ssh/<nom_clé>.pub
   ```

#### Ajout de la clé publique à son compte
   Connectez-vous au GitLab d'INP-net à l'adresse <https://git.inpt.fr/users/sign_in>.
   
   Ajouter la clé (petit bouton en haut à droite) sur <https://git.inpt.fr/-/profile/keys>. Il faut copier-coller le résultat du cat dans le champ "Clé" et la tour est joué.

## Exercice 0 : Étape préliminaire

**RÉALISEZ VOS AJOUTS DE FICHIERS ET/OU DOSSIER DANS TEST**

Clonez le dépôt sur votre ordinateur pour pouvoir travailler dessus
```bash
git clone git@git.inpt.fr:net7/git-exercises.git
```

Pour le bien de ce pauvre répertoire, vous allez créer une branche à votre nom :
```bash
git checkout -b <nouvelle-branche>
```
Cela va permettre de réaliser vos premiers pas en toute sécurité 👍

## Exercice 1 : Premiers pas

1. Créez un nouveau fichier en utilisant votre éditeur préféré : vim
   *(peut-être une formation vim un jour o^o)*
   
   (`i` pour éditer, `echap` quand c'est fini, `:w` pour sauvegarder et `:q` pour quitter)
   
   ```bash
   vim <fichier>
   ```

2. Après tout changement significatif, ajouter les ou les nouveaux fichiers

   ```bash
   git add <fichier ou dossier>
   ```

3. Validez les nouveaux fichiers

   ```bash
   git commit -m "<message-de-commit-significatif>"
   ```

4. (optionnel) Examinez l'état de votre repo avec `git status` et/ou git fetch avant et après avoir ajouté une modification au fichier précédent (refaire le git add et tralala)

   ```bash
   git status
   git fetch
   ```

5. (optionnel) Effectuez d'autres commits et consultez le journal

   ```bash
   git log
   ```

6. Commitez tout ce que vous avez fait jusqu'à présent

   ```bash
   git add .
   git commit -m "<message-de-commit-significatif>"
   ```
   **!!! `git add .` c'est bourrin, à utiliser avec précaution quand on est pas seul sur le repo !!!**

7.  Poussez les commits sur le serveur

      ```bash
      git push
      ```

## Exercice 2 : Branchement et fusion

1. Créez une nouvelle branche à partir de votre première branche (oui, on commence à freestyle)

   ```bash
   git checkout -b <nouvelle-branche>
   ```

2. Éditez votre nouveau fichier et validez le résultat (comme dans l'exercice 1, c'est pour voir si tu suis ou si tu tapes juste les commandes en no brain mode 👀)

3. Retournez à la première branche

   ```bash
   git checkout <nom-premiere_branche>
   ```

4. Fusionner `nouvelle_branche` vers `premiere_branche`

   ```bash
   git merge new_branch
   ```

5. Maintenant, créez des commits conflictuels dans `nouvelle_branche` et `premiere_branche` et essayez de les fusionner. Notez que les marqueurs de résolution de conflit ressembleront à ceci.

   ```bash
   <<<<<<< HEAD
   Voici la nouvelle ligne dans premiere_branche
   =======
   C'est la nouvelle ligne dans la branche
   >>>>>>> branche
   ```

6. Résolvez le conflit (c'est-à-dire, éditez les marqueurs de conflit pour qu'ils correspondent à ce que vous voulez que le fichier soit) et livrez le résultat. Utilisez `git log` pour voir les commits résultants sur la branche main.

## Exercice 3 : Collaboration

Maintenant que vous vous êtes bien amusés sur votre branche, il est temps de demander aux admins du repo git d'accepter vos "magnifiques" ajouts. 

Pour ce faire, il vous suffit de suivre la procédure en ligne sur votre navigateur en cliquant sur le bouton "Create Pull Request" 🎉🎉🎉

## Remerciements

- <https://github.com/martinjrobins/exercise>
- <https://git.inpt.fr/Zil0/formation-git>
- <https://wiki.python.org/moin/SimplePrograms>
- <https://github.com/pypa/sampleproject>
- <https://git.inpt.fr/fainsil/git-exercises>
- DeepL pour la trad en français psk @fainsil l'avait écrit en anglais et @lebihae avait la flemme de traduire à la main
   - je me disais que c'était vachement bien écrit aussi 🙃
